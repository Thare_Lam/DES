"""
    Author: Thare_Lam
"""
import rule as R


def htb(ov):
    bv = bin(int(ov, 16))[2:].replace('L', '')
    nbv = (len(ov) * 4 - len(bv)) * '0' + bv
    rd = [int(i) for i in nbv]
    return rd


def bth(ov):
    rd = [hex(int(ov[i:i + 4], 2))[2:] for i in range(0, len(ov), 4)]
    return ''.join(rd)


def stl(s):
    rd = [int(i) for i in s]
    return rd


def xor(a, b):
    rd = [0 if a[i] == b[i] else 1 for i in range(0, len(a))]
    return rd


def rea(d, r):
    rd = [d[i - 1] for i in r]
    return rd


def gsk(key):
    key = rea(key, R.pc1)
    keylist = [key[R.m[i]:28] + key[0:R.m[i]] + key[R.m[i]+28:56] + key[28:R.m[i]+28]
                   for i in range(0, 16)]
    subkey = [rea(keylist[i], R.pc2) for i in range(0, len(keylist))]
    return subkey


def f(r, k):
    r = rea(r, R.rre)
    km = xor(r, k)
    km = [km[6 * i:6 * (i + 1)] for i in range(0, 8)]
    sb = []
    for i in range(0, 8):
        row = km[i][0] * 2 + km[i][5]
        line = km[i][1] * 8 + km[i][2] * 4 + km[i][3] * 2 + km[i][4]
        sb.append(hex(R.sbox[i][row * 16 + line])[2:])
    sb = htb(''.join(sb))
    sb = rea(sb, R.sre)
    return sb


def od(data, keyson, mode):
    data = rea(data, R.drs)
    l = [int(i) for i in data[0:32]]
    r = [int(i) for i in data[32:]]
    if mode == 1:
        keyson.reverse()
    for i in range(0, 16):
        t = r
        r = xor(l, f(r, keyson[i]))
        l = t
    dd = rea(r + l, R.dre)
    t = [str(i) for i in dd]
    return bth(''.join(t))


def des(key, data, mode):
    subkey = gsk(key)
    ct = od(data, subkey, mode)
    if mode == 0:
        print 'After Encrypt: ',
    else:
        print 'After Decipher: ',
    print ct


if __name__ == '__main__':
    while 1:
        print '-' * 30
        key = raw_input("Input Key(Length = 16) : ")
        if len(key) != 16:
            print 'Invaild Input'
            continue
        data = raw_input("Input Data(Length = 16): ")
        if len(data) != 16:
            print 'Invaild Input'
            continue
        mode = int(raw_input("Input mode(0 for Encrypt and 1 for Decipher): "))
        if mode != 0 and mode != 1:
            print 'Invaild Input'
            continue
        key = htb(key)
        data = htb(data)
        des(key, data, mode)
